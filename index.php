


<!DOCTYPE html>
<html lang="fr-FR">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css" type="text/css">
</head>
<body>

<?php



// appel du model API et des données à charger
require_once('model/ApiFetcher.php');

$fileJsonApiC26 = new ApiFetcher("https://www.lecoledunumerique.fr/wp-json/wp/v2/apprenants?per_page=100");
$data = $fileJsonApiC26->getApprenticeData();


// appel du moteur de Template et passage des données
require_once('model/TemplateEngine.php');
$templates = new TemplateEngine('view');
$trombi = $templates->render('students.php', array('dataStudent' => $data));

   
    
    
// affichage des données
echo $trombi


?>

</body>
</html>


<!DOCTYPE html>
<html lang="fr-FR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Trombinoscope P2#DWWM</title>
    <link rel="stylesheet" href="../style.css" type="text/css">
    <script src="https://kit.fontawesome.com/edc9370e24.js" crossorigin="anonymous"></script>
</head>


<body>
    <header class="header">
        <img src="img/logoedn.png" class="edn"> 
    </header>
    <div class="imgtxtcontainer">
        <img src="img/p2.png" class="p2">
        <div class="container-txt">
            <h1 class="trombi">LE TROMBINOSCOPE</h1>
            <p class="sous-texte">des apprenants de la formation développeur web et mobile</p>
        </div>
    </div>
</body>

</html>
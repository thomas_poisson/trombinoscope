<?php
include 'view/header.php';
?>
<div class="student">

<?php
foreach ((array)$dataStudent as $student){
    ?>
    <div class="studentinfos">
        <h2> <?php echo $student->title->rendered?> </h2>

        <p> <?php echo $student->promotion->name?> </p>
        <div class="extphotos">
            <img id="photos" src="<?php echo $student->featured_media?>">
            <div class="extraita">
                <div class="extraitb"><?php echo $student->yoast_head_json->og_description?></div>
            </div>
        </div>
        <div class ="infos">
            <ul class="comp"> <?php 
            foreach ($student->competences as $competence){ 
                    echo '<li class="competences">'. $competence->name . '</li>';
                }?></ul>
            <ul class="logos">
                <li><a class="liens" target="_blank" href="<?php echo $student->portfolio ?>"><i class="fas fa-briefcase"></i></a></li>
                <li><a class="liens" target="_blank" href="<?php echo $student->linkedin ?>"><i class="fab fa-linkedin"></i></a></li>
                <li><a class="liens" target="_blank" href="<?php echo $student->cv?>"><i class="fas fa-download"></i></a></li></li>
            </ul>
        </div>
    </div>
 <?php  
}
?>
</div>
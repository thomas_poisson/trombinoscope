<!-- Model -->
<?php
    class ApiFetcher {
        public $apiC26;

        public function __construct($apiC26) 
        {
            $this->apiC26 = $apiC26;
        }

        public function getApprenticeData() {
            $data = $this->fetchData();
            return $this->decode($data);
        }

        private function fetchData() {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $this->apiC26);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);

            $data = curl_exec($curl);

            return $data;
        }

        private function decode($jsonString) {
            return json_decode($jsonString);
        }

    }
    

    
